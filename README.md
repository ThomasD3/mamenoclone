# MameNoClone

This tool was built to remove the clones from a MAME folder.

I wrote it for my personal use, added some functionalities to make it usable by others,
however, it hasn't been tested anywhere else, so use at your own risk :)

The tool just requires to pass the path of your Mame 0.78 folder and will create a 'clones'
folder and move all the game clones there. No file is ever deleted.

Since the binary is 12kb, I have decided to include it in the distribution. You will need
the .NET 4.7 framework installed for this program to work.



